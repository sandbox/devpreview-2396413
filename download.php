<?php

/**
 * @file
 * Download and extract Bootstrap3
 */

/**
 * Check Bootstrap dist
 * 
 * @param string $version
 * @param string $path
 * @return boolean
 */
function bootstrap3_download_bootstrap_check($version, $path) {
    $version = bootstrap3_get_latest_version($version);
    $files   = array(
        'css' . DIRECTORY_SEPARATOR . 'bootstrap.css',
        'css' . DIRECTORY_SEPARATOR . 'bootstrap.min.css',
        'css' . DIRECTORY_SEPARATOR . 'bootstrap-theme.css',
        'css' . DIRECTORY_SEPARATOR . 'bootstrap-theme.min.css',
        'fonts' . DIRECTORY_SEPARATOR . 'glyphicons-halflings-regular.eot',
        'fonts' . DIRECTORY_SEPARATOR . 'glyphicons-halflings-regular.svg',
        'fonts' . DIRECTORY_SEPARATOR . 'glyphicons-halflings-regular.ttf',
        'fonts' . DIRECTORY_SEPARATOR . 'glyphicons-halflings-regular.woff',
        'js' . DIRECTORY_SEPARATOR . 'bootstrap.js',
        'js' . DIRECTORY_SEPARATOR . 'bootstrap.min.js'
    );
    $path    = drupal_realpath($path . DIRECTORY_SEPARATOR . $version);
    if (file_exists($path . DIRECTORY_SEPARATOR . 'dist')) {
        $path = $path . DIRECTORY_SEPARATOR . 'dist';
    } elseif (file_exists($path . DIRECTORY_SEPARATOR . 'bootstrap-' . $version . '-dist')) {
        $path = $path . DIRECTORY_SEPARATOR . 'bootstrap-' . $version . '-dist';
    } else {
        return false;
    }
    foreach ($files as $file) {
        if (!file_exists(drupal_realpath($path . DIRECTORY_SEPARATOR . $file))) {
            return false;
        }
    }
    return true;
}

/**
 * Prepare Bootstrap temp and extracted directories
 * 
 * @param string $version
 * @param string $path
 * @return NULL
 */
function bootstrap3_download_bootstrap_prepare($version, $path) {
    if (bootstrap3_download_bootstrap_check($version, $path)) {
        drupal_set_message(t('Bootstrap already downloaded.'), 'status');
        return;
    }

    $version = bootstrap3_get_latest_version($version);
    $config  = \Drupal::config('bootstrap3.settings');
    $config->clear('download_path_temp');
    $config->save();

    if (!file_prepare_directory($path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
        drupal_set_message(t('Can not create Bootstrap directory'), 'error');
        return;
    }

    $temp = file_directory_temp();
    if (!is_writable($temp)) {
        if (is_writable($path)) {
            $temp = $path . DIRECTORY_SEPARATOR . 'tmp';
        } else {
            drupal_set_message(t('Can not write to temporary directory'), 'error');
            return;
        }
    }
    $temp = $temp . DIRECTORY_SEPARATOR . 'bootstrap-' . $version . '-dist.zip';
    $config->set('download_path_temp', $temp);
    $config->save();
}

/**
 * Download Bootstrap dist
 * 
 * @param string $version
 * @param string $path
 * @return NULL
 */
function bootstrap3_download_bootstrap($version, $path) {
    if (bootstrap3_download_bootstrap_check($version, $path)) {
        return;
    }
    $config = \Drupal::config('bootstrap3.settings');
    if (!$config->get('download_path_temp')) {
        return;
    }

    $version = bootstrap3_get_latest_version($version);
    $temp    = system_retrieve_file(
            'https://github.com/twbs/bootstrap/releases/download/v' . $version . '/bootstrap-' . $version . '-dist.zip', $config->get('download_path_temp')
    );

    if (!$temp) {
        drupal_set_message(t('Can not download Bootstrap'), 'error');
        $config->clear('download_path_temp');
        $config->save();
        return;
    }
    $config->set('download_path_temp', $temp);
    $config->save();
}

/**
 * Extract Bootstrap dist
 * 
 * @param string $version
 * @param string $path
 * @return NULL
 * @throws Exception
 */
function bootstrap3_download_bootstrap_extract($version, $path) {
    if (bootstrap3_download_bootstrap_check($version, $path)) {
        return;
    }
    $config = \Drupal::config('bootstrap3.settings');
    if (!$config->get('download_path_temp')) {
        return;
    }

    $temp = $config->get('download_path_temp');
    try {
        $archiver = archiver_get_archiver($temp);
        if (!$archiver) {
            throw new Exception(t('Cannot extract %file, not a valid archive.', array('%file' => $temp)));
        }
        $archiver->extract($path . DIRECTORY_SEPARATOR . bootstrap3_get_latest_version($version));
    } catch (Exception $e) {
        drupal_set_message($e->getMessage(), 'error');
    }

    drupal_unlink($temp);
    $config->clear('download_path_temp');
}

/**
 * Finish
 * 
 * @param string $version
 * @param string $path
 */
function bootstrap3_download_bootstrap_finished($version, $path) {
    if (bootstrap3_download_bootstrap_check($version, $path)) {
        $config = \Drupal::config('bootstrap3.settings');
        $config->set('loader', 'download');
        $config->set('download_version', $version);
        $config->set('download_path', $path);
        $config->save();
        drupal_set_message(t('The configuration options have been saved.'
                        . ' Please <a href="@clear_cache">clear the cache</a> so that the new information is loaded into Drupal.', array(
            '@clear_cache' => '/admin/config/development/performance'
        )));
    } else {
        drupal_set_message(t('Download error'), 'error');
    }
}
