<?php

/**
 * @file
 * Contains Drupal\bootstrap3\Form\SettingsForm.
 */

namespace Drupal\bootstrap3\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Serialization\Yaml;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Core\Site\Settings;

/**
 * Configure Bootstrap settings for this site.
 */
class SettingsForm extends ConfigFormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'bootstrap3_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        // Get anything we need form the base class.
        $form   = parent::buildForm($form, $form_state);
        $config = $this->config('bootstrap3.settings');

        $versionElement = array(
            '#type'        => 'select',
            '#title'       => $this->t('Bootstrap version'),
            '#description' => $this->t('Select which Bootstrap version to use by default.'),
            '#options'     => $this->prepareBootstrapVersions()
        );

        $form['general'] = array(
            '#type'  => 'fieldset',
            '#title' => t('General'),
        );

        $form['general']['compression'] = array(
            '#type'          => 'radios',
            '#title'         => $this->t('Bootstrap compression level'),
            '#default_value' => $config->get('compression'),
            '#options'       => array(
                1 => $this->t('Production (minified)'),
                0 => $this->t('Development (uncompressed)')
            ),
            '#required'      => TRUE
        );

        $form['general']['javascript'] = array(
            '#type'          => 'checkbox',
            '#title'         => $this->t('Enable Bootstrap JavaScript plugins'),
            '#default_value' => $config->get('javascript')
        );

        $form['general']['loader'] = array(
            '#type'          => 'radios',
            '#title'         => $this->t('Bootstrap lodear type'),
            '#description'   => $this->t('Select which Bootstrap loader type.'),
            '#default_value' => $config->get('loader'),
            '#options'       => array(
                'cdn'      => $this->t('Bootstrap CDN'),
                'builtin'  => $this->t('Built-in (version: ' . $this->getBuiltInVersion() . ')'),
                'download' => $this->t('Download Bootstrap'),
                'custom'   => $this->t('Custom')
            ),
            '#required'      => TRUE
        );

        $form['download'] = array(
            '#type'   => 'fieldset',
            '#title'  => t('Download Bootstrap'),
            '#states' => array(
                'visible' => array(
                    ':input[name="loader"]' => array('value' => 'download'),
                )
            )
        );

        $form['download']['download_path'] = array(
            '#type'          => 'textfield',
            '#title'         => $this->t('Download path'),
            '#maxlength'     => 255,
            '#default_value' => $config->get('download_path') ? $config->get('download_path') : Settings::get('file_public_path', conf_path() . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'bootstrap3'),
            '#states'        => array(
                'required' => array(
                    ':input[name="loader"]' => array('value' => 'download'),
                )
            )
        );

        $form['download']['download_version'] = array(
            '#type'          => 'select',
            '#title'         => $this->t('Bootstrap version'),
            '#description'   => $this->t('Select which Bootstrap version to use by default.'),
            '#options'       => $this->prepareBootstrapVersions(false),
            '#default_value' => $config->get('download_version'),
            '#states'        => array(
                'required' => array(
                    ':input[name="loader"]' => array('value' => 'download'),
                )
            )
        );

        $form['cdn'] = array(
            '#type'   => 'fieldset',
            '#title'  => t('Bootstrap CDN'),
            '#states' => array(
                'visible' => array(
                    ':input[name="loader"]' => array('value' => 'cdn'),
                )
            )
        );

        $form['cdn']['cdn'] = array(
            '#type'          => 'select',
            '#title'         => $this->t('Select CDN'),
            '#description'   => $this->t('Select which Bootstrap CDN.'),
            '#default_value' => $config->get('cdn'),
            '#options'       => bootstrap3_get_available_cdn(),
            '#states'        => array(
                'required' => array(
                    ':input[name="loader"]' => array('value' => 'cdn'),
                )
            )
        );

        $form['cdn']['cdn_version'] = array(
            '#type'          => 'select',
            '#title'         => $this->t('Bootstrap version'),
            '#description'   => $this->t('Select which Bootstrap version to use by default.'),
            '#options'       => $this->prepareBootstrapVersions(),
            '#default_value' => $config->get('cdn_version'),
            '#states'        => array(
                'required' => array(
                    ':input[name="loader"]' => array('value' => 'cdn'),
                )
            )
        );

        $form['custom'] = array(
            '#type'         => 'fieldset',
            '#title'        => t('Custom URL'),
            '#states'       => array(
                'visible' => array(
                    ':input[name="loader"]' => array('value' => 'custom'),
                )
            ),
            '#field_suffix' => $this->t('Use relative URI (e.g. "/path/bootstrap/css/bootstrap.css") or absolute URL (e.g. "//static.example.com/bootstrap/css/bootstrap.css").')
        );

        $form['custom']['custom_url_css'] = array(
            '#type'          => 'textfield',
            '#title'         => $this->t('Path to bootstrap.css'),
            '#maxlength'     => 255,
            '#default_value' => $config->get('custom_url_css') ? $config->get('custom_url_css') : '/' . drupal_get_path('module', 'bootstrap3') . '/assets/vendor/bootstrap/css/bootstrap.css',
            '#states'        => array(
                'required' => array(
                    ':input[name="loader"]'      => array('value' => 'custom'),
                    ':input[name="compression"]' => array('value' => 0)
                ),
                'visible'  => array(
                    ':input[name="compression"]' => array('value' => 0)
                )
            )
        );

        $form['custom']['custom_url_js'] = array(
            '#type'          => 'textfield',
            '#title'         => $this->t('Path to bootstrap.js'),
            '#maxlength'     => 255,
            '#default_value' => $config->get('custom_url_js') ? $config->get('custom_url_js') : '/' . drupal_get_path('module', 'bootstrap3') . '/assets/vendor/bootstrap/js/bootstrap.js',
            '#states'        => array(
                'required' => array(
                    ':input[name="loader"]'      => array('value' => 'custom'),
                    ':input[name="javascript"]'  => array('checked' => true),
                    ':input[name="compression"]' => array('value' => 0)
                ),
                'visible'  => array(
                    ':input[name="javascript"]'  => array('checked' => true),
                    ':input[name="compression"]' => array('value' => 0)
                )
            )
        );

        $form['custom']['custom_url_css_min'] = array(
            '#type'          => 'textfield',
            '#title'         => $this->t('Path to bootstrap.min.css'),
            '#maxlength'     => 255,
            '#default_value' => $config->get('custom_url_css_min') ? $config->get('custom_url_css_min') : '/' . drupal_get_path('module', 'bootstrap3') . '/assets/vendor/bootstrap/css/bootstrap.min.css',
            '#states'        => array(
                'required' => array(
                    ':input[name="loader"]'      => array('value' => 'custom'),
                    ':input[name="compression"]' => array('value' => 1)
                ),
                'visible'  => array(
                    ':input[name="compression"]' => array('value' => 1)
                )
            )
        );

        $form['custom']['custom_url_js_min'] = array(
            '#type'          => 'textfield',
            '#title'         => $this->t('Path to bootstrap.min.js'),
            '#maxlength'     => 255,
            '#default_value' => $config->get('custom_url_js_min') ? $config->get('custom_url_js_min') : '/' . drupal_get_path('module', 'bootstrap3') . '/assets/vendor/bootstrap/js/bootstrap.min.js',
            '#states'        => array(
                'required' => array(
                    ':input[name="loader"]'      => array('value' => 'custom'),
                    ':input[name="javascript"]'  => array('checked' => true),
                    ':input[name="compression"]' => array('value' => 1)
                ),
                'visible'  => array(
                    ':input[name="javascript"]'  => array('checked' => true),
                    ':input[name="compression"]' => array('value' => 1)
                )
            )
        );

        $form['usage'] = array(
            '#type'  => 'fieldset',
            '#title' => t('Usage')
        );

        $form['usage']['usage_type'] = array(
            '#type'          => 'radios',
            '#title'         => $this->t('Usage'),
            '#default_value' => $config->get('usage_type'),
            '#options'       => array(
                'library' => $this->t('As library'),
                'site'    => $this->t('All site pages'),
                'all'     => $this->t('Site pages and admin area'),
                'admin'   => $this->t('Only admin area')
            ),
            '#required'      => TRUE
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $config = $this->config('bootstrap3.settings');

        $config->set('compression', $form_state->getValue('compression'));
        if ($form_state->getValue('loader') != 'download') {
            $config->set('loader', $form_state->getValue('loader'));
        }
        $config->set('javascript', $form_state->getValue('javascript'));
        $config->set('usage_type', $form_state->getValue('usage_type'));
        $config->save();

        switch ($form_state->getValue('loader')) {
            case 'cdn':
                $config->set('cdn_version', $form_state->getValue('cdn_version'));
                $config->set('cdn', $form_state->getValue('cdn'));
                $config->save();
                break;
            case 'download':
                $batch = array(
                    'operations' => array(
                        array('bootstrap3_download_bootstrap_prepare', array($form_state->getValue('download_version'), $form_state->getValue('download_path'))),
                        array('bootstrap3_download_bootstrap', array($form_state->getValue('download_version'), $form_state->getValue('download_path'))),
                        array('bootstrap3_download_bootstrap_extract', array($form_state->getValue('download_version'), $form_state->getValue('download_path'))),
                        array('bootstrap3_download_bootstrap_finished', array($form_state->getValue('download_version'), $form_state->getValue('download_path'))),
                    ),
                    'file'       => drupal_get_path('module', 'bootstrap3') . DIRECTORY_SEPARATOR . 'download.php',
                );
                batch_set($batch);
                return;
            case 'custom':
                if ($config->get('compression') == 0) {
                    if ($config->get('javascript') == 1) {
                        $config->set('custom_url_js', $form_state->getValue('custom_url_js'));
                    }
                    $config->set('custom_url_css', $form_state->getValue('custom_url_css'));
                } else {
                    if ($config->get('javascript') == 1) {
                        $config->set('custom_url_js_min', $form_state->getValue('custom_url_js_min'));
                    }
                    $config->set('custom_url_css_min', $form_state->getValue('custom_url_css_min'));
                }
                $config->save();
                break;
        }

        drupal_set_message($this->t('The configuration options have been saved.'
                        . ' Please <a href="@clear_cache">clear the cache</a> so that the new information is loaded into Drupal.', array(
                    '@clear_cache' => '/admin/config/development/performance'
        )));
    }

    private function prepareBootstrapVersions($latest = true) {
        $result = array();
        if ($latest) {
            $result['3'] = $this->t('Latest version');
        }
        foreach (bootstrap3_get_available_versions() as $version => $versions) {
            $_result = array();
            if ($latest) {
                $_result[$version] = $version . '-latest';
            }
            foreach ($versions as $_version) {
                $_result[$_version] = $_version;
            }
            $result[$version . '.x'] = $_result;
        }
        return $result;
    }

    private function getBuiltInVersion() {
        try {
            $libraries = Yaml::decode(file_get_contents(drupal_realpath(drupal_get_path('module', 'bootstrap3') . DIRECTORY_SEPARATOR . 'bootstrap3.libraries.yml')));
            if (array_key_exists('bootstrap', $libraries) && array_key_exists('version', $libraries['bootstrap'])) {
                return $libraries['bootstrap']['version'];
            } else {
                return 'Undefined';
            }
        } catch (InvalidDataTypeException $e) {
            return 'Undefined';
        }
    }

}
