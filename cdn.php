<?php

return array(
    'names' => array(
        'maxcdn'    => 'MaxCDN (Bootstrap CDN)',
        'microsoft' => 'Microsoft Ajax CDN',
        'yandex'    => 'Yandex',
        'cdnjs'     => 'cdnjs'
    ),
    'urls'  => array(
        'maxcdn'    => array(
            'css'     => '//maxcdn.bootstrapcdn.com/bootstrap/::VERSION::/css/bootstrap.css',
            'css.min' => '//maxcdn.bootstrapcdn.com/bootstrap/::VERSION::/css/bootstrap.min.css',
            'js'      => '//maxcdn.bootstrapcdn.com/bootstrap/::VERSION::/js/bootstrap.js',
            'js.min'  => '//maxcdn.bootstrapcdn.com/bootstrap/::VERSION::/js/bootstrap.min.js'
        ),
        'microsoft' => array(
            'css'     => '//ajax.aspnetcdn.com/ajax/bootstrap/::VERSION::/css/bootstrap.css',
            'css.min' => '//ajax.aspnetcdn.com/ajax/bootstrap/::VERSION::/css/bootstrap.min.css',
            'js'      => '//ajax.aspnetcdn.com/ajax/bootstrap/::VERSION::/bootstrap.js',
            'js.min'  => '//ajax.aspnetcdn.com/ajax/bootstrap/::VERSION::/bootstrap.min.js'
        ),
        'yandex'    => array(
            'css'     => '//yastatic.net/bootstrap/::VERSION::/css/bootstrap.css',
            'css.min' => '//yastatic.net/bootstrap/::VERSION::/css/bootstrap.min.css',
            'js'      => '//yastatic.net/bootstrap/::VERSION::/js/bootstrap.js',
            'js.min'  => '//yastatic.net/bootstrap/::VERSION::/js/bootstrap.min.js'
        ),
        'cdnjs'     => array(
            'css'     => '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/::VERSION::/css/bootstrap.css',
            'css.min' => '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/::VERSION::/css/bootstrap.min.css',
            'js'      => '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/::VERSION::/js/bootstrap.js',
            'js.min'  => '//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/::VERSION::/js/bootstrap.min.js'
        )
    )
);
